import org.junit.Test;
import Messages.*;
import java.util.Date;

import static org.junit.Assert.*;
/**
 * Created by swc on 9/23/16.
 */
public class StreamStocksMessageTester {

    @Test
    public void StartStreamMessage_TestAddEncodeAndDecode()
    {
        StreamStocksMessage m1 = new StreamStocksMessage();
        assertEquals(0, m1.Count());

        m1.Add("ABAX");
        assertEquals(1, m1.Count());

        m1.Add("AMZN");
        assertEquals(2, m1.Count());

        m1.Add("AAPL");
        assertEquals(3, m1.Count());

        m1.Add(null);
        assertEquals(3, m1.Count());

        m1.Add("");
        assertEquals(3, m1.Count());

        m1.Add("   ");
        assertEquals(3, m1.Count());

        assertNotNull(m1.getSymbols());

        byte[] encodedMessage = m1.Encode();
        assertNotNull(encodedMessage);
        assertEquals(20, encodedMessage.length);

        StreamStocksMessage m2 = StreamStocksMessage.Decode(encodedMessage);
        assertNotNull(m2);
        assertNotNull(m2.getSymbols());
        assertEquals(3, m2.getSymbols().toArray().length);
        assertEquals("ABAX", m2.getSymbols().toArray()[0]);
        assertEquals("AMZN", m2.getSymbols().toArray()[1]);
        assertEquals("AAPL", m2.getSymbols().toArray()[2]);
    }

    @Test
    public void StartStreamMessage_TestMessageWithNoSymbols()
    {
        StreamStocksMessage m1 = new StreamStocksMessage();
        assertEquals(0, m1.Count());

        byte[] encodedMessage = m1.Encode();
        assertNotNull(encodedMessage);
        assertEquals(2, encodedMessage.length);

        StreamStocksMessage m2 = StreamStocksMessage.Decode(encodedMessage);
        assertNotNull(m2);
        assertNotNull(m2.getSymbols());
        assertEquals(0, m2.getSymbols().toArray().length);
    }

    @Test
    public void StartStreamMessage_TestMessageWithNullSymbols()
    {
        StreamStocksMessage m1 = new StreamStocksMessage();
        byte[] encodedMessage = m1.Encode();
        assertNotNull(encodedMessage);
        assertEquals(2, encodedMessage.length);

        StreamStocksMessage m2 = StreamStocksMessage.Decode(encodedMessage);
        assertNotNull(m2);
        assertNotNull(m2.getSymbols());
        assertEquals(0, m2.getSymbols().toArray().length);
    }

    @Test
    public void StartStreamMessage_TestDecodingOfBadBytes()
    {
        byte[] encodedMessage = new byte[0];
        StreamStocksMessage m2 = StreamStocksMessage.Decode(encodedMessage);
        assertNull(m2);

        encodedMessage = new byte[] { 1 };
        m2 = StreamStocksMessage.Decode(encodedMessage);
        assertNull(m2);

        encodedMessage = new byte[] { 0, 1, 65 };
        m2 = StreamStocksMessage.Decode(encodedMessage);
        assertNull(m2);

    }

}

﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Common
{
    /// <summary>
    /// Sample, Partial Communicator
    /// 
    /// The following is a partial example of a class
    /// </summary>
    public class Communicator
    {
        private UdpClient _myUdpClient;
        private bool _isMonitoring;

        // The following property will hold the communication end point for the simulator.  The object that creates
        // and setup an instance of the communicator, can use the EndPointParser to create an IPEndPoint from a string
        // like "127.0.0.1:12099" or from a host name and port number.  See EndPointParser class
        public IPEndPoint RemoteEndPoint { get; set; }

        /// <summary>
        /// This method just returns a status string, i.e., Monitoring if the communication is trying to receiving
        /// messages from the simulator.
        /// </summary>
        public string Status => _isMonitoring ? "Monitoring" : "";

        // TODO: This class may need to contain other properties, like something that references your stocks,
        // TODO: a portfolio of stock, or TODO: a delegate to a method that updates your stock portfolio.

        /// <summary>
        /// This method contains sample code for create a Datagram Socket (i.e., UdpClient) that can communicator with
        /// the simulator.  It also contain sample code for starting a thread tries to receive incoming message.
        /// </summary>
        public void Start()
        {
            IPEndPoint localEp = new IPEndPoint(IPAddress.Any, 0);
            _myUdpClient = new UdpClient(localEp);

            _isMonitoring = true;
            ThreadPool.QueueUserWorkItem(Monitoring, null);
        }

        /// <summary>
        /// This method contains sample code for stopping the UdpClient.  Also, setting the _isMonitoring variable
        /// should stop the Monitoring thread
        /// </summary>
        public void Stop()
        {
            _isMonitoring = false;

            if (_myUdpClient != null)
            {
                _myUdpClient.Close();
                _myUdpClient = null;
            }
        }

        /// <summary>
        /// This is a partial implementation of the main method for the Monitoring thread.  It will first
        /// create and send StreamStocksMessage, and then go into a recieve loop until _isMonitoring is
        /// false
        /// </summary>
        /// <param name="state"></param>
        private void Monitoring(object state)
        {
            // TODO: Make sure the system is a corret state
            
            StreamStocksMessage startMessage = new StreamStocksMessage();
            // TODO: For each stock symbol in the the portfolio, add it to the startMessage
            Send(startMessage);

            while (_isMonitoring)
            {
                TickerMessage message = Receive(1000);
                
                // TODO: Process the incoming message, i.e., update the stock specified in the message
            }
        }


        /// <summary>
        /// This method shows how to send a message through the UdpClient to RemoteEndPoint
        /// </summary>
        /// <param name="message"></param>
        private void Send(StreamStocksMessage message)
        {
            if (message == null) return;

            byte[] bytesToSend = message.Encode();

            try
            {
                _myUdpClient.Send(bytesToSend, bytesToSend.Length, RemoteEndPoint);
            }
            catch (Exception err)
            {
                // TODO: handle the error
            }

            return;
        }

        /// <summary>
        /// This method receives a single TickerMessage within some time limit.  If no message
        /// comes in within the time limit, then it returns a null.  Used by Monitoring method.
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns></returns>
        private TickerMessage Receive(int timeout = 0)
        {
            TickerMessage message = null;

            byte[] receivedBytes = ReceiveBytes(timeout);
            if (receivedBytes != null && receivedBytes.Length > 0)
                message = TickerMessage.Decode(receivedBytes);

            return message;
        }

        /// <summary>
        /// This method receives a byte array from the UpdClient, within some time limit.  Used by
        /// the Receive method
        /// </summary>
        /// <param name="timeout"></param>
        /// <returns></returns>
        private byte[] ReceiveBytes(int timeout)
        {
            byte[] receivedBytes = null;

            _myUdpClient.Client.ReceiveTimeout = timeout;
            IPEndPoint senderEndPoint = new IPEndPoint(IPAddress.Any, 0);
            try
            {
                receivedBytes = _myUdpClient.Receive(ref senderEndPoint);
            }
            catch (SocketException err)
            {
                if (err.SocketErrorCode != SocketError.TimedOut && err.SocketErrorCode != SocketError.Interrupted)
                {
                    // TODO: Handle an unexpected error
                }
            }
            catch (Exception err)
            {
                // TODO: Handle an unexpected error
            }

            return receivedBytes;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;


namespace MulticastComm
{
    public delegate void DisplayHandler(string displayMessage);
    public abstract class MulticastCommunicator
    {
        protected UdpClient MyUdpClient;
        private Task _myTask;
        private bool _keepGoing;
        

        public string Name { get; protected set; }
        public IPAddress GroupAddress { get; set; }
        public int GroupPort { get; set; }
        public DisplayHandler DisplayMethod { get; set; }
        public int TimeoutInMilliseconds { get; set; }

        public void Start()
        {
            _keepGoing = true;
            _myTask = Task.Run(new Action(Run));
        }

        public void Stop()
        {
            _keepGoing = false;
            _myTask.Wait();
        }

        private void Run()
        {
            SetupUdpClient();

            DoStuff();

            TearDownClient();
        }

        protected abstract void SetupUdpClient();
        protected abstract void DoStuff();
        protected abstract void TearDownClient();

        protected void Send(List<Message> messages, IPEndPoint ep)
        {
            if (messages == null || ep == null) return;

            foreach (Message msg in messages)
            {
                Thread.Sleep(1000);
                DisplayMethod?.Invoke($"Sending {msg.Text} to {ep}");
                byte[] bytes = msg.Encode();
                MyUdpClient.Send(bytes, bytes.Length, ep);
            }
        }

        protected IPEndPoint Receive(int numberOfFinalMessages)
        {
            IPEndPoint ep = null;
            int finallyMessagesRecieved = 0;
            while (finallyMessagesRecieved < numberOfFinalMessages && _keepGoing)
            {
                try
                {
                    ep = new IPEndPoint(IPAddress.Any, 0);
                    byte[] bytes = MyUdpClient.Receive(ref ep);
                    if (bytes != null)
                    {
                        Message msg = Message.Decode(bytes);
                        DisplayMethod?.Invoke($"Recieved {msg.Text} from {ep}");
                        if (msg is FinalMessage)
                            finallyMessagesRecieved++;
                    }
                }
                catch
                {
                    // ignore                   
                }
            }
            return ep;
        }
    }
}

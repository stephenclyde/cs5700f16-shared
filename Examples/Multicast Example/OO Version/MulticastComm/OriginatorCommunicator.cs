﻿using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace MulticastComm
{
    public class OriginatorCommunicator : MulticastCommunicator
    {
        public List<Message> MessagesToSend { get; set; }
        public int NumberOfResponsesToWaitFor { get; set; }

        protected override void SetupUdpClient()
        {
            // Create a UDP Client and have it bound to any available port
            MyUdpClient = new UdpClient(0) {Client = {ReceiveTimeout = TimeoutInMilliseconds}};
        }

        protected override void DoStuff()
        {
            IPEndPoint remoteEndPoint = new IPEndPoint(GroupAddress, GroupPort);
            Send(MessagesToSend, remoteEndPoint);
            Receive(NumberOfResponsesToWaitFor);
        }

        protected override void TearDownClient()
        {
            MyUdpClient.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MulticastComm;

namespace Target
{
    public class Program
    {
        public static void Main(string[] args)
        {
            TargetCommunicator originator = new TargetCommunicator()
            {
                GroupAddress = IPAddress.Parse("224.3.2.1"),
                GroupPort = 5000,
                TimeoutInMilliseconds = 1000,
                ReplyMessages = new List<Message>()
                {
                    new Message() {Text = "Hello Originator"},
                    new Message() {Text = "You too"},
                    new FinalMessage()
                },
                DisplayMethod = ConsoleDisplay
            };

            originator.Start();

            Console.ReadKey();

            originator.Stop();
        }


        public static void ConsoleDisplay(string text)
        {
            Console.WriteLine(text);
        }

    }
}

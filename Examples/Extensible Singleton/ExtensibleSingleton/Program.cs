﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensibleSingleton
{
    public class Program
    {
        static void Main(string[] args)
        {
            // Functionality from the base Singleton class
            ExtendedSingleton.Instance.AnotherInstanceMethod(10);
            ExtendedSingleton.Instance.AnotherInstanceMethod(13);
            ExtendedSingleton.Instance.AnotherInstanceMethod(52);
            foreach (int i in ExtendedSingleton.Instance.MyInts)
                Console.Write($"{i} ");

            Console.WriteLine("");
            Console.WriteLine("");

            // Functionality from the specialization
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("hello");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("This simple program illustrate how a singleton can be extended in C#.");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("The base class provide some functionality that will common to all");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("specialization.");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("A specializationk of that base class can added to the instance's");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod(" capabilities.");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("Note that the essential part of the singleton pattern, i.e.,");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("the guarentee that only one instance of the class exists is");
            ExtendedSingleton.Instance.YetAnotherInstanceMethod("encapsulated in one place.");

            foreach (string s in ExtendedSingleton.Instance.MyStrings)
            {
                Console.WriteLine(s);
            }

            Console.ReadKey();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensibleSingleton
{
    public abstract class BaseSingleton
    {
        private static BaseSingleton _instance;
        private static readonly object MyLock = new object();

        private readonly List<int> _interestData = new List<int>();          // Sample state information for instance
        public List<int> MyInts => _interestData; 

        /// <summary>
        /// This method is the heart of the Singleton.  It encapsulates the logic that guarentees only one instance of the class
        /// </summary>
        /// <param name="specialization"></param>
        /// <returns></returns>
        protected static BaseSingleton GetInstance(Type specialization)
        {
            lock (MyLock)
            {
                if (_instance == null)
                    _instance = Activator.CreateInstance(specialization) as BaseSingleton;
            }
            return _instance;
        }

        public void AnInstanceMethod()
        {
            // Interesting stuff done here, e.g.,            
            _interestData.Reverse();                        
        }

        public void AnotherInstanceMethod(int x)
        {
            // More interesting stuff done here
            _interestData.Add(x);
        }
    }
}

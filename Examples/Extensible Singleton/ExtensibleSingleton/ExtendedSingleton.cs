﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExtensibleSingleton
{
    public class ExtendedSingleton : BaseSingleton
    {
        public static ExtendedSingleton Instance => GetInstance(typeof (ExtendedSingleton)) as ExtendedSingleton;

        private readonly List<string> _moreStateData = new List<string>();
        public List<string> MyStrings => _moreStateData;

        public void YetAnotherInstanceMethod(string s)
        {
            _moreStateData.Add(s);
        }
    }
}

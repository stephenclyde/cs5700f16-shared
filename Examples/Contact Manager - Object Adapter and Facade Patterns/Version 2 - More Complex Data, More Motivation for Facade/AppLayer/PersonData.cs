﻿using System;
using System.Collections.Generic;
using AppLayer.CSVAdapters;

namespace AppLayer
{
    public class PersonData
    {
        private readonly List<Person> _personData = new List<Person>();
         
        private bool isLoaded = false;

        public string NameFilename { get; set; }
        public string PhoneFilename { get; set; }
        public List<Person> Data
        {
            get
            {
                if (!isLoaded)
                    Load();
                return _personData;
            }
        }

        private void Load()
        {
            ValidFileNames();

            List<Name> names = LoadNames();
            List<Phone> phones = LoadPhones();

            _personData.Clear();
            foreach (Name name in names)
            {
                Person person = new Person() {Name = name};
                person.AddPhoneNumbers(phones.FindAll(p => p.PersonId == person.Id));
                _personData.Add(person);
            }
        }

        public void Save()
        {
            ValidFileNames();
            SaveNames();
            SavePhones();
        }


        private List<Name> LoadNames()
        {
            List<Name> names = new List<Name>();

            INameReader nameReader = new NameStreamReader(NameFilename);

            while (!nameReader.AtEnd)
            {
                Name name = nameReader.Read();
                if (name != null)
                    names.Add(name);
            }
            nameReader.Close();

            return names;
        }

        private List<Phone> LoadPhones()
        {
            List<Phone> phones = new List<Phone>();

            IPhoneReader phoneReader = new PhoneStreamReader(PhoneFilename);

            while (!phoneReader.AtEnd)
            {
                Phone phone = phoneReader.Read();
                if (phone != null)
                    phones.Add(phone);
            }
            phoneReader.Close();

            return phones;
        }

        private void SaveNames()
        {
            INameWriter writer = new NameStreamWriter(NameFilename);

            foreach (Person person in _personData)
                writer.Write(person.Name);
            writer.Close();
        }

        private void SavePhones()
        {
            IPhoneWriter writer = new PhoneStreamWriter(PhoneFilename);

            foreach (Person person in _personData)
                foreach (Phone phoneNumber in person.PhoneNumbers)
                    writer.Write(phoneNumber);
            writer.Close();
        }

        private void ValidFileNames()
        {
            if (string.IsNullOrWhiteSpace(NameFilename))
                throw new ApplicationException("The Name Filename cannot be null or blank");

            if (string.IsNullOrWhiteSpace(PhoneFilename))
                throw new ApplicationException("The Phone Filename cannot be null or blank");
        }
    }
}


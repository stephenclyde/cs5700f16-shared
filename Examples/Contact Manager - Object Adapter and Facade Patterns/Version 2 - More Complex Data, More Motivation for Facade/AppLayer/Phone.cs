﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer
{
    public class Phone
    {
        public int PersonId { get; set; }
        public string PhoneNumber { get; set; }
    }
}

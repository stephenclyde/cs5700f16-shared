﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppLayer
{
    interface INameReader
    {
        bool AtEnd { get; }
        Name Read();
        void Close();
    }
}

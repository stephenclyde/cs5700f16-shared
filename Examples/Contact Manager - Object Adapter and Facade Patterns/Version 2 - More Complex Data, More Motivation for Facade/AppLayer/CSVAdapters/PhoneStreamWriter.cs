﻿using System.IO;

namespace AppLayer.CSVAdapters
{
    public class PhoneStreamWriter : IPhoneWriter
    {
        private readonly StreamWriter _writer;

        public PhoneStreamWriter(string filename)
        {
            try
            {
                _writer = new StreamWriter(filename);
            }
            catch
            {
                // ignored
            }
        }

        public void Write(Phone phone)
        {
            if (_writer != null && phone != null)
            {
                _writer.WriteLine("{0},{1}", phone.PersonId, phone.PhoneNumber.Replace(",", "&comma&"));
            }
        }

        public void Close()
        {
            _writer?.Close();
        }
    }
}

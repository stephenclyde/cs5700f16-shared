﻿
using System;
using System.IO;

namespace AppLayer.CSVAdapters
{
    /// <summary>
    /// This is an object adapter for StreamReader, where the Read method returns a Person instead of reading just a line of text
    /// </summary>
    public class PhoneStreamReader : IPhoneReader
    {
        private readonly StreamReader _reader;

        public PhoneStreamReader(string filename)
        {
            try
            {
                _reader = new StreamReader(filename);
            }
            catch
            {
                // ignored
            }
        }

        public bool AtEnd => (_reader == null || _reader.EndOfStream);

        public Phone Read()
        {
            Phone phone = null;
            try
            {
                if (_reader!=null && !_reader.EndOfStream)
                {
                    string recordData = _reader.ReadLine();
                    string[] fields = recordData?.Split(',');
                    if (fields?.Length == 2)
                    {
                        phone = new Phone()
                        {
                            PersonId = Convert.ToInt32(fields[0]),
                            PhoneNumber = fields[1].Replace("&comma&", ",")
                        };
                    }
                }
            }
            catch
            {
                // ignored
            }
            return phone;
        }

        public void Close()
        {
            _reader?.Close();
        }
    }
}

﻿
using System;
using System.IO;

namespace AppLayer.CSVAdapters
{
    /// <summary>
    /// This is an object adapter for StreamReader, where the Read method returns a Person instead of reading just a line of text
    /// </summary>
    public class NameStreamReader : INameReader
    {
        private readonly StreamReader _reader;

        public NameStreamReader(string filename)
        {
            try
            {
                _reader = new StreamReader(filename);
            }
            catch
            {
                // ignored
            }
        }

        public bool AtEnd => (_reader == null || _reader.EndOfStream);

        public Name Read()
        {
            Name name = null;
            try
            {
                if (_reader!=null && !_reader.EndOfStream)
                {
                    string recordData = _reader.ReadLine();
                    string[] fields = recordData?.Split(',');
                    if (fields?.Length == 3)
                    {
                        name = new Name()
                        {
                            Id = Convert.ToInt32(fields[0]),
                            FirstName = fields[1].Replace("&comma&",","),
                            LastName = fields[2].Replace("&comma&", ",")
                        };
                    }
                }
            }
            catch
            {
                // ignored
            }
            return name;
        }

        public void Close()
        {
            _reader?.Close();
        }
    }
}

﻿using System.IO;

namespace AppLayer.CSVAdapters
{
    public class PersonStreamWriter : IPersonWriter
    {
        private readonly StreamWriter _writer;

        public PersonStreamWriter(string filename)
        {
            try
            {
                _writer = new StreamWriter(filename);
            }
            catch
            {
                // ignored
            }
        }

        public void Write(Person person)
        {
            if (_writer != null && person != null)
            {
                string firstName = person.FirstName.Replace(",", "&comma&");
                string lastName = person.LastName.Replace(",", "&comma&");
                string phone = person.Phone.Replace(",", "&comma&");
                _writer.WriteLine("{0},{1},{2}", firstName, lastName, phone);
            }
        }

        public void Close()
        {
            _writer?.Close();
        }
    }
}

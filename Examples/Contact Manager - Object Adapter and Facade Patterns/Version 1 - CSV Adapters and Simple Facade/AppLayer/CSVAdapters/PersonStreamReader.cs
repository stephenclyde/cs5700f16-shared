﻿
using System.IO;

namespace AppLayer.CSVAdapters
{
    /// <summary>
    /// This is an object adapter for StreamReader, where the Read method returns a Person instead of reading just a line of text
    /// </summary>
    public class PersonStreamReader : IPersonReader
    {
        private readonly StreamReader _reader;

        public PersonStreamReader(string filename)
        {
            try
            {
                _reader = new StreamReader(filename);
            }
            catch
            {
                // ignored
            }
        }

        public bool AtEnd => (_reader == null || _reader.EndOfStream);

        public Person Read()
        {
            Person person = null;
            try
            {
                if (_reader!=null && !_reader.EndOfStream)
                {
                    string recordData = _reader.ReadLine();
                    string[] fields = recordData?.Split(',');
                    if (fields?.Length == 3)
                    {
                        person = new Person()
                        {
                            FirstName = fields[0].Replace("&comma&",","),
                            LastName = fields[1].Replace("&comma&", ","),
                            Phone = fields[2].Replace("&comma&", ",")
                        };
                    }
                }
            }
            catch
            {
                // ignored
            }
            return person;
        }

        public void Close()
        {
            _reader?.Close();
        }
    }
}

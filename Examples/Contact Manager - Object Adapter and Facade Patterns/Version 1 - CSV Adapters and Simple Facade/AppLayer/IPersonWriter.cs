﻿namespace AppLayer
{
    interface IPersonWriter
    {
        void Write(Person person);
        void Close();
    }
}

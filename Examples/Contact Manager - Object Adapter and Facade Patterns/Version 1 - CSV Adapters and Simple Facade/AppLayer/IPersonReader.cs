﻿namespace AppLayer
{
    /// <summary>
    /// IPersonReader
    /// 
    /// This is the base class in an object adpater pattern instance that defines an person reader API.
    /// </summary>
    interface IPersonReader
    {
        bool AtEnd { get; }
        Person Read();
        void Close();
    }
}

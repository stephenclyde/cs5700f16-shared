﻿using System;
using System.Collections.Generic;
using AppLayer.CSVAdapters;

namespace AppLayer
{
    public class PersonData
    {
        private readonly List<Person> _personData = new List<Person>();
        private bool isLoaded = false;

        public string Filename { get; set; }
        public List<Person> Data
        {
            get
            {
                if (!isLoaded)
                    Load();
                return _personData;
            }
        }

        public void Save()
        {
            if (string.IsNullOrWhiteSpace(Filename))
                throw new ApplicationException("Filename cannot be null or blank");

            PersonStreamWriter writer = new PersonStreamWriter(Filename);
            foreach (Person person in _personData)
                writer.Write(person);
            writer.Close();
        }

        private void Load()
        {
            if (string.IsNullOrWhiteSpace(Filename))
                throw new ApplicationException("Filename cannot be null or blank");

            _personData.Clear();
            PersonStreamReader personReader = new PersonStreamReader(Filename);

            while (!personReader.AtEnd)
            {
                Person person = personReader.Read();
                if (person != null)
                    _personData.Add(person);
            }
            personReader.Close();
        }

    }
}


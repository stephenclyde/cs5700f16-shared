﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressions.Functions
{
    public class RoundFunction : Function
    {
        public override double Execute(List<double> parameters)
        {
            double result = 0;
            if (parameters!=null && parameters.Count == 1)
                result = Math.Round(parameters[0]);
            return result;
        }

        public override double GetEstimatedTime(List<Double> parameters)
        {
            return 1;
        }

        public override double GetEstimatedValue(List<Double> parameters)
        {
            return Execute(parameters);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressions
{
    public class Constant : IExpression
    {
        public double Value { get; set; }

        public override double Evaluate(Interpretation interpretation)
        {
            return Value;
        }

        public override double EstimateTime(Interpretation interpretation)
        {
            return 0;
        }

        public override double EstimateValue(Interpretation interpretation)
        {
            return Value;
        }

        public override string PrefixNotation
        {
            get { return Value.ToString(); }
        }

        public override string InfixNotation
        {
            get { return Value.ToString(); }
        }

        public override string PostfixNotation
        {
            get { return Value.ToString(); }
        }

        public override IExpression OptimizedExpression
        {
            get { return this.MemberwiseClone() as Constant; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Expressions.Functions;
using Expressions.Operators;

namespace Expressions
{
    public class FunctionExpression : IExpression
    {
        public Function Function { get; set; }
        public List<IExpression> Parameters
        {
            get { return subExpressions; }
            set
            {
                subExpressions = value;
                if (subExpressions == null)
                    subExpressions = new List<IExpression>();
            }
        }

        public override double Evaluate(Interpretation interpretation)
        {
            double result = 0;
            if (Function != null && Parameters.Count > 0)
            {
                List<double> parameterValues = new List<double>();
                foreach (IExpression param in Parameters)
                    parameterValues.Add(param.EstimateValue(interpretation));
                result = Function.Execute(parameterValues);
            }
            return result;
        }

        public override double EstimateTime(Interpretation interpretation)
        {
            double result = 0;
            if (Function != null && Parameters != null && Parameters.Count > 0)
            {
                double parameterTimeEstimates = 0;
                List<double> parameterValueEstimates = new List<double>();
                foreach (IExpression param in Parameters)
                {
                    parameterTimeEstimates += param.EstimateTime(interpretation);
                    parameterValueEstimates.Add(param.EstimateValue(interpretation));
                }

                result = parameterTimeEstimates + Function.GetEstimatedTime(parameterValueEstimates);
            }
            return result;
        }

        public override double EstimateValue(Interpretation interpretation)
        {
            double result = 0;
            if (Function != null && Parameters.Count > 0)
            {
                List<double> parameterValues = new List<double>();
                foreach (IExpression param in Parameters)
                    parameterValues.Add(param.EstimateValue(interpretation));

                result = Function.GetEstimatedValue(parameterValues);
            }

            return result;
        }

        public override string PrefixNotation
        {
            get
            {
                string result = string.Empty;
                if (Function != null && Parameters.Count > 0)
                {
                    result = Function.Label;

                    List<double> parameterValues = new List<double>();
                    foreach (IExpression param in Parameters)
                        result += " " + param.PrefixNotation;
                }
                return result;
            }
        }

        public override string InfixNotation
        {
            get
            {
                string result = string.Empty;
                if (Function != null && Parameters.Count > 0)
                {
                    result = Function.Label;
                    string delimeter = "(";
                    List<double> parameterValues = new List<double>();
                    foreach (IExpression param in Parameters)
                    {
                        result += delimeter + param.PrefixNotation;
                        delimeter = ", ";
                    }
                    result += ")";
                }
                return result;
            }
        }

        public override string PostfixNotation
        {
            get
            {
                string result = string.Empty;
                if (Function != null && Parameters.Count > 0)
                {
                    List<double> parameterValues = new List<double>();
                    foreach (IExpression param in Parameters)
                        result += param.PrefixNotation + " ";
                    result += Function.Label;
                }
                return result;
            }
        }

        public override IExpression OptimizedExpression
        {
            get
            {
                List<IExpression> newParameters = new List<IExpression>();

                if (Function != null && Parameters.Count > 0)
                {
                    foreach (IExpression param in Parameters)
                        newParameters.Add(param.OptimizedExpression);
                }

                FunctionExpression newExpression = new FunctionExpression()
                                                    {
                                                        Function = Function,
                                                        Parameters = newParameters
                                                    };
                return newExpression;
            }
        }
    }
}

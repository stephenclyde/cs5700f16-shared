﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressions.Operators
{
    public abstract class BinaryOperator
    {
        public abstract double Execute(double operand1, double operand2);
        public string Label { get { return ToString(); } }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Expressions.Operators
{
    public class FloorOperator : UnaryOperator
    {
        public override double Execute(double operand)
        {
            return Math.Floor(operand);
        }

        public override string ToString()
        {
            return "\u230B";
        }
    }
}

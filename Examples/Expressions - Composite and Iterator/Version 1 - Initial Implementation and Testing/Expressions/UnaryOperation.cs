﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Expressions.Operators;

namespace Expressions
{
    public class UnaryOperation : IExpression
    {
        public UnaryOperator Operator { get; set; }
        public IExpression Operand { get; set; }

        public double Evaluate(Interpretation interpretation)
        {
            double result = 0;
            if (Operator != null && Operand != null)
                result = Operator.Execute(Operand.Evaluate(interpretation));
            return result;
        }

        public double EstimateTime(Interpretation interpretation)
        {
            double result = 1;
            if (Operator != null && Operand != null)
                result += Operand.EstimateTime(interpretation);
            return result;
        }

        public double EstimateValue(Interpretation interpretation)
        {
            double result = 0;
            if (Operator != null && Operand != null)
                result = Operand.EstimateValue(interpretation);
            return result;
        }

        public string PrefixNotation
        {
            get { return string.Format("{0} {1}", Operator.Label, Operand.PrefixNotation); }

        }

        public string InfixNotation
        {
            get { return string.Format("({0} {1})", Operator.Label, Operand.PrefixNotation); }
        }

        public string PostfixNotation
        {
            get { return string.Format("{0} {1}", Operand.PrefixNotation, Operator.Label); }
        }

        public IExpression OptimizedExpression
        {
            get
            {
                IExpression result = null;
                if (Operand is Constant)
                {
                    Interpretation i = new Interpretation();
                    result = new Constant() { Value = Operator.Execute(Operand.Evaluate(i)) };
                }
                else
                {
                    result = new UnaryOperation()
                        {
                            Operand = Operand.OptimizedExpression,
                            Operator = Operator
                        };
                }
                return result;
            }
        }
    }
}

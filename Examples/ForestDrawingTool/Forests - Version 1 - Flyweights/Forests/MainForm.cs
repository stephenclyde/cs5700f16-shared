﻿using System;
using System.Drawing;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using AppLayer.DrawingComponents;

namespace Forests
{
    // NOTE: There some design problems with this class

    public partial class MainForm : Form
    {
        private const int NormalWidth = 80;
        private const int NormalHeight = 80;

        private readonly TreeFactory _factory = new TreeFactory();
        private Drawing _drawing = new Drawing();
        private string _currentTreeResource;
        private float _currentScale = 1;

        private Bitmap _imageBuffer;
        private Graphics _imageBufferGraphics;
        private Graphics _panelGraphics;
       
        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            refreshTimer.Start();
        }

        private void refreshTimer_Tick(object sender, EventArgs e)
        {
            DisplayDrawing();
        }

        private void DisplayDrawing()
        {
            if (_imageBuffer == null)
            {
                _imageBuffer = new Bitmap(drawingPanel.Width, drawingPanel.Height);
                _imageBufferGraphics = Graphics.FromImage(_imageBuffer);
                _panelGraphics = drawingPanel.CreateGraphics();
            }

            if (_drawing.Draw(_imageBufferGraphics))
                _panelGraphics.DrawImageUnscaled(_imageBuffer, 0, 0);
        }

        private void newButton_Click(object sender, EventArgs e)
        {
            _drawing = new Drawing();
            DisplayDrawing();
        }

        private void ClearOtherSelectedTools(ToolStripButton ignoreItem)
        {
            foreach (ToolStripItem item in drawingToolStrip.Items)
            {
                ToolStripButton toolButton = item as ToolStripButton;
                if (toolButton != null && item!=ignoreItem && toolButton.Checked )
                    toolButton.Checked = false;
            }
        }

        private void pointerButton_Click(object sender, EventArgs e)
        {
            ClearOtherSelectedTools(pointerButton);
            _currentTreeResource = string.Empty;
        }

        private void treeButton_Click(object sender, EventArgs e)
        {
            ToolStripButton button = sender as ToolStripButton;
            ClearOtherSelectedTools(button);

            if (button != null && button.Checked)
                _currentTreeResource = $"Forests.Graphics.{button.Text}.png";
            else
                _currentTreeResource = string.Empty;
        }

        private void drawingPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(_currentTreeResource))
            {
                Size treeSize = new Size()
                {
                    Width = Convert.ToInt16(Math.Round(NormalWidth*_currentScale, 0)),
                    Height = Convert.ToInt16(Math.Round(NormalHeight*_currentScale, 0))
                };
                Point treeLocation = new Point(e.Location.X - treeSize.Width/2, e.Location.Y - treeSize.Height/2);

                TreeExtrinsicState extrinsicState = new TreeExtrinsicState()
                {
                    Location = treeLocation,
                    Size = treeSize
                };
                Tree tree = _factory.GetTree(_currentTreeResource, typeof(Program), extrinsicState);
                _drawing.Add(tree);
            }
        }

        private void scale_Leave(object sender, EventArgs e)
        {
            _currentScale = ConvertToFloat(scale.Text, 0.01F, 99.0F, 1);
            scale.Text = _currentScale.ToString(CultureInfo.InvariantCulture);
        }

        private float ConvertToFloat(string text, float min, float max, float defaultValue)
        {
            float result = defaultValue;
            if (!string.IsNullOrWhiteSpace(text))
            {
                if (!float.TryParse(text, out result))
                    result = defaultValue;
                else
                    result = Math.Max(min, Math.Min(max, result));
            }
            return result;
        }

        private void scale_TextChanged(object sender, EventArgs e)
        {
            _currentScale = ConvertToFloat(scale.Text, 0.01F, 99.0F, 1);
        }
    }
}

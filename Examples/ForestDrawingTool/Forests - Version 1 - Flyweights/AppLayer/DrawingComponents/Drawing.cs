﻿using System.Collections.Generic;
using System.Drawing;

namespace AppLayer.DrawingComponents
{
    // NOTE: This class at least one encapsulation problem that hurts its coupling and cohesion

    public class Drawing
    {
        private readonly List<Tree> _trees = new List<Tree>();
        private bool _isDirty;
        private readonly object _myLock = new object();

        public Tree SelectedTree { get; set; }
        public bool IsDirty => _isDirty;

        public void Add(Tree tree)
        {
            if (tree != null)
            {
                lock (_myLock)
                {
                    _trees.Add(tree);
                    _isDirty = true;
                }
            }
        }

        public void RemoveTree(Tree tree)
        {
            if (tree != null)
            {
                lock (_myLock)
                {
                    if (SelectedTree == tree)
                        SelectedTree = null;
                    _trees.Remove(tree);
                    _isDirty = true;
                }
            }
        }

        public void SelectTreeAtPosition(Point location)
        {
            if (SelectedTree != null)
                SelectedTree.IsSelected = false;

            SelectedTree = FindTreeAtPosition(location);

            if (SelectedTree != null)
                SelectedTree.IsSelected = true;

            _isDirty = true;
        }

        public Tree FindTreeAtPosition(Point location)
        {
            Tree result;
            lock (_myLock)
            {
                result = _trees.FindLast(t => location.X >= t.Location.X &&
                                              location.X < t.Location.X + t.Size.Width &&
                                              location.Y >= t.Location.Y &&
                                              location.Y < t.Location.Y + t.Size.Height);
                /*
                    The amount method call with a lamba predicate is the same as the following:
                for (var i=_trees.Count-1; i>=0; i--)
                {
                    if (location.X >= _trees[i].Location.X &&
                        location.X < _trees[i].Location.X + _trees[i].Size.Width &&
                        location.Y >= _trees[i].Location.Y &&
                        location.Y < _trees[i].Location.Y + _trees[i].Size.Height)
                    {
                        result = _trees[i];
                        break;
                    }
                }
                */
            }
            return result;
        }

        public bool Draw(Graphics graphics)
        {
            bool didARedraw = false;
            lock (_myLock)
            {
                if (_isDirty)
                {
                    graphics.Clear(Color.White);
                    foreach (Tree t in _trees)
                        t.Draw(graphics);
                    _isDirty = false;
                    didARedraw = true;
                }
            }
            return didARedraw;
        }

    }
}

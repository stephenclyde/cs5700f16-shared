﻿using System;
using System.Collections.Generic;

namespace AppLayer.DrawingComponents
{
    public class TreeFactory
    {
        private readonly Dictionary<string, TreeWithIntrinsicState> _sharedTrees = new Dictionary<string, TreeWithIntrinsicState>();

        public Tree GetTree(string treeType, Type referenceTypeForAssembly, TreeExtrinsicState extrinsicState)
        {
            TreeWithIntrinsicState treeWithIntrinsicState;
            if (_sharedTrees.ContainsKey(treeType))
                treeWithIntrinsicState = _sharedTrees[treeType];
            else
            {
                treeWithIntrinsicState = new TreeWithIntrinsicState();
                treeWithIntrinsicState.LoadFromResource(treeType, referenceTypeForAssembly);
                _sharedTrees.Add(treeType, treeWithIntrinsicState);
            }

            return new TreeWithAllState(treeWithIntrinsicState, extrinsicState);
        }
    }
}
